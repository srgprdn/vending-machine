# README #

- Dependencies: **maven, jdk 11**

##### INSTALLATION ####
- Build project
````
mvn clean install
````
- launch on port 8080
````
java -jar target\mvpmatch-0.0.1-SNAPSHOT.jar
````
- explore swagger docs at
````
http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config
````
