package com.example.mvpmatch.service.vending;

import com.example.mvpmatch.exception.NotEnoughCashAvailableException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class GreedyCashDispenserCalculatorTest {

    /**
     * The greedy algorithm will solve optimally for the default set of coins. Changing this set might result in
     * sub-optimal number of coins dispersed or might not even solve the problem at all.
     */
    private static final Map<Long, Long> defaultDenominations = Map.of(
            5L, 100L,
            10L, 100L,
            20L, 100L,
            50L, 100L,
            100L, 100L
    );
    private final GreedyCashDispenserCalculator victim = new GreedyCashDispenserCalculator();

    public static Stream<Arguments> exceptionalCasesData() {
        return Stream.of(
                arguments(100L,
                        Map.of(
                                20L, 0L,
                                11L, 0L,
                                10L, 5L,
                                1L, 5L)),
                // IN A NON-GREEDY IMPL SHOULD RETURN 11-3
                arguments(33L,
                        Map.of(
                                20L, 1L,
                                11L, 3L,
                                10L, 5L,
                                1L, 0L)),
                arguments(55L,
                        Map.of(
                                5L, 0L,
                                10L, 100L,
                                20L, 100L,
                                50L, 100L,
                                100L, 100L)),
                arguments(-5L,
                        Map.of(
                                5L, 10L,
                                10L, 100L,
                                20L, 100L,
                                50L, 100L,
                                100L, 100L))
        );
    }

    public static Stream<Arguments> happyFlowData() {
        return Stream.of(
                arguments(33L,
                        Map.of(
                                20L, 1L,
                                11L, 5L,
                                10L, 5L,
                                1L, 5L),
                        // SUBOPTIMAL
                        Map.of(
                                20L, 1L,
                                11L, 1L,
                                1L, 2L)),
                // OPTIMAL
                // Map.of(11, 3)},
                arguments(25L,
                        Map.of(
                                25L, 0L,
                                15L, 1L,
                                10L, 1L,
                                5L, 2L,
                                1L, 5L),
                        Map.of(
                                15L, 1L,
                                10L, 1L)),

                arguments(100,
                        Map.of(
                                50L, 2L,
                                15L, 1L,
                                10L, 1L,
                                5L, 2L,
                                1L, 5L),
                        Map.of(
                                50L, 2L)),

                arguments(0,
                        Map.of(
                                50L, 2L,
                                15L, 1L,
                                10L, 1L,
                                5L, 2L,
                                1L, 5L),
                        Map.of()),
                arguments(55,
                        defaultDenominations,
                        Map.of(
                                50L, 1L,
                                5L, 1L)),
                arguments(255,
                        defaultDenominations,
                        Map.of(
                                100L, 2L,
                                50L, 1L,
                                5L, 1L
                        )),
                arguments(160,
                        defaultDenominations,
                        Map.of(
                                100L, 1L,
                                50L, 1L,
                                10L, 1L
                        ))
        );
    }

    public static Stream<Arguments> casesWithRestrictedNumberOfCoins() {
        return Stream.of(
                arguments(55,
                        Map.of(
                                5L, 11L,
                                10L, 0L,
                                20L, 0L,
                                50L, 0L,
                                100L, 0L),
                        Map.of(
                                5L, 11L)),
                arguments(255,
                        Map.of(
                                5L, 100L,
                                10L, 100L,
                                20L, 10L,
                                50L, 0L,
                                100L, 0L),
                        Map.of(
                                20L, 10L,
                                10L, 5L,
                                5L, 1L)),
                arguments(300,
                        Map.of(
                                5L, 2L,
                                10L, 1L,
                                20L, 4L,
                                50L, 2L,
                                100L, 1L),
                        Map.of(
                                5L, 2L,
                                10L, 1L,
                                20L, 4L,
                                50L, 2L,
                                100L, 1L))
                );
    }

    @ParameterizedTest
    @MethodSource("happyFlowData")
    public void shouldDispenseMinimumNumberOfCashItems(long totalToDispense, Map<Long, Long> availableCash, Map<Long, Long> expectedResult) throws NotEnoughCashAvailableException {
        Map<Long, Long> result = victim.calculateCashToDispense(totalToDispense, availableCash);
        assertEquals(expectedResult.entrySet().size(), result.entrySet().size());
        String message = String.format("Expected %s but was %s", expectedResult, result);
        for (Map.Entry<Long, Long> expectedEntry : expectedResult.entrySet()) {
            assertTrue(result.containsKey(expectedEntry.getKey()), message);
            assertEquals(result.get(expectedEntry.getKey()), expectedEntry.getValue(), message);
        }
    }

    @ParameterizedTest
    @MethodSource("casesWithRestrictedNumberOfCoins")
    public void shouldDispenseMinimumNumberOfAvailableCashItems(long totalToDispense, Map<Long, Long> availableCash, Map<Long, Long> expectedResult) throws NotEnoughCashAvailableException {
        Map<Long, Long> result = victim.calculateCashToDispense(totalToDispense, availableCash);
        assertEquals(expectedResult.entrySet().size(), result.entrySet().size());
        String message = String.format("Expected %s but was %s", expectedResult, result);
        for (Map.Entry<Long, Long> expectedEntry : expectedResult.entrySet()) {
            assertTrue(result.containsKey(expectedEntry.getKey()), message);
            assertEquals(result.get(expectedEntry.getKey()), expectedEntry.getValue(), message);
        }
    }

    @ParameterizedTest
    @MethodSource("exceptionalCasesData")
    public void shouldSignalInsufficientCash(long totalToDispense, Map<Long, Long> availableCash) {
        assertThrows(NotEnoughCashAvailableException.class, () -> {
            victim.calculateCashToDispense(totalToDispense, availableCash);
        });
    }
}