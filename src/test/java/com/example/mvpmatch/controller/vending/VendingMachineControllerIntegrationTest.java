package com.example.mvpmatch.controller.vending;

import com.example.mvpmatch.model.Product;
import com.example.mvpmatch.model.User;
import com.example.mvpmatch.model.UserRole;
import com.example.mvpmatch.repository.ProductRepository;
import com.example.mvpmatch.repository.UserRepository;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.example.mvpmatch.validation.ValidationMessages.INSUFFICIENT_BALANCE_MESSAGE;
import static com.example.mvpmatch.validation.ValidationMessages.INSUFFICIENT_PRODUCT_AMOUNT_MESSAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class VendingMachineControllerIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private User bob;
    private User alice;

    private Product banana;
    private Product juice;

    private MockMvc mockMvc;

    private static String buyProductRequestContent(long productId, long amount) {
        return String.format("{\n" +
                "    \"productId\": %d,\n" +
                "    \"amount\": %d\n" +
                "}", productId, amount);
    }

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        User bob = new User();
        bob.setRole(UserRole.SELLER);
        bob.setDeposit(10);
        bob.setPassword(bCryptPasswordEncoder.encode("bob"));
        bob.setUsername("bob");
        this.bob = userRepository.save(bob);

        User alice = new User();
        alice.setRole(UserRole.BUYER);
        alice.setDeposit(5);
        alice.setPassword(bCryptPasswordEncoder.encode("alice"));
        alice.setUsername("alice");
        this.alice = userRepository.save(alice);

        Product product1 = new Product();
        product1.setAmountAvailable(10);
        product1.setProductName("Juice");
        product1.setCost(15);
        product1.setUser(bob);
        juice = productRepository.save(product1);

        Product product2 = new Product();
        product2.setAmountAvailable(1);
        product2.setProductName("Banana");
        product2.setCost(5);
        product2.setUser(bob);
        banana = productRepository.save(product2);
    }

    @AfterEach
    public void tearDown() throws InterruptedException {
        productRepository.deleteById(juice.getId());
        productRepository.deleteById(banana.getId());
        userRepository.deleteById(bob.getId());
        userRepository.deleteById(alice.getId());
    }

    @Test
    public void shouldBeAbleToDeposit() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/deposit")
                        .with(httpBasic("alice", "alice"))
                        .content("{" +
                                "    \"depositAmount\": 10" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
        assertEquals(15, userRepository.findByUsername("alice").getDeposit());
    }

    @Test
    public void shouldBeAbleToReset() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/reset")
                        .with(httpBasic("alice", "alice"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
        assertEquals(0, userRepository.findByUsername("alice").getDeposit());
    }

    @Test
    public void sellersCannotReset() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/reset")
                        .with(httpBasic("bob", "bob"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
        assertEquals(10, userRepository.findByUsername("bob").getDeposit());
    }

    @Test
    public void sellersCannotDeposit() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/deposit")
                        .with(httpBasic("bob", "bob"))
                        .content("{" +
                                "    \"depositAmount\": 10" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
        assertEquals(10, userRepository.findByUsername("bob").getDeposit());
    }

    @Test
    public void shouldBeAbleToBuy() throws Exception {
        long bananaId = banana.getId();
        mockMvc.perform(MockMvcRequestBuilders.post("/buy")
                        .with(httpBasic("alice", "alice"))
                        .content(buyProductRequestContent(bananaId, 1))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.amount", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productName", Is.is(banana.getProductName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.remainingDeposit", Is.is(0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.transactionCost", Is.is(5)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        assertEquals(0, userRepository.findByUsername("alice").getDeposit());
        assertEquals(15, userRepository.findByUsername("bob").getDeposit());
    }

    @Test
    public void shouldNotAllowSpendingOverBalance() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/buy")
                        .with(httpBasic("alice", "alice"))
                        .content(buyProductRequestContent(juice.getId(), 1))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.reason", Is.is(INSUFFICIENT_BALANCE_MESSAGE)));
    }

    @Test
    public void shouldNotAllowBuyingOverProductStock() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/deposit")
                        .with(httpBasic("alice", "alice"))
                        .content("{" +
                                "    \"depositAmount\": 100" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.post("/buy")
                        .with(httpBasic("alice", "alice"))
                        .content(buyProductRequestContent(juice.getId(), 11))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.reason", Is.is(INSUFFICIENT_PRODUCT_AMOUNT_MESSAGE)));
    }
}