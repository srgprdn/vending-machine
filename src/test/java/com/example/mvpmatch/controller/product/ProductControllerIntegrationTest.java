package com.example.mvpmatch.controller.product;

import com.example.mvpmatch.model.Product;
import com.example.mvpmatch.model.User;
import com.example.mvpmatch.model.UserRole;
import com.example.mvpmatch.repository.ProductRepository;
import com.example.mvpmatch.repository.UserRepository;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@ExtendWith(SpringExtension.class)
@Transactional
@SpringBootTest
public class ProductControllerIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private User seller;

    private User anotherSeller;

    private User buyer;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        Random random = new Random();
        User seller = new User();
        seller.setRole(UserRole.SELLER);
        seller.setDeposit(0);
        seller.setPassword(bCryptPasswordEncoder.encode("seller"));
        seller.setUsername("seller" + random.nextInt(99999));
        this.seller = userRepository.save(seller);

        User seller1 = new User();
        seller1.setRole(UserRole.SELLER);
        seller1.setDeposit(0);
        seller1.setPassword(bCryptPasswordEncoder.encode("seller"));
        seller1.setUsername("seller" + random.nextInt(99999));
        this.anotherSeller = userRepository.save(seller1);

        User buyer = new User();
        buyer.setRole(UserRole.BUYER);
        buyer.setDeposit(0);
        buyer.setPassword(bCryptPasswordEncoder.encode("buyer"));
        buyer.setUsername("buyer" + random.nextInt(99999));
        this.buyer = userRepository.save(buyer);
    }

    @Test
    public void shouldCreateProductOnPOST() throws Exception {
        String product = "{\n" +
                "    \"amountAvailable\": 4,\n" +
                "    \"cost\": 100,\n" +
                "    \"productName\": \"laboris tempor\"\n" +
                "}";
        MvcResult createdProduct = mockMvc.perform(MockMvcRequestBuilders.post("/product")
                        .with(httpBasic(this.seller.getUsername(), "seller"))
                        .content(product)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String productId = createdProduct.getResponse().getContentAsString();
        Product updatedProductFromPersistence = productRepository.getById(Long.valueOf(productId));
        assertEquals(4, updatedProductFromPersistence.getAmountAvailable());
        assertEquals(100, updatedProductFromPersistence.getCost());
        assertEquals("laboris tempor", updatedProductFromPersistence.getProductName());
    }

    @Test
    public void shouldUpdateProduct() throws Exception {
        MvcResult createdProduct = mockMvc.perform(MockMvcRequestBuilders.post("/product")
                        .with(httpBasic(this.seller.getUsername(), "seller"))
                        .content("{\n" +
                                "    \"amountAvailable\": 4,\n" +
                                "    \"cost\": 100,\n" +
                                "    \"productName\": \"laboris tempor\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String productId = createdProduct.getResponse().getContentAsString();
        mockMvc.perform(MockMvcRequestBuilders.put("/product/" + productId)
                        .with(httpBasic(this.seller.getUsername(), "seller"))
                        .content("{\n" +
                                "    \"amountAvailable\": 10,\n" +
                                "    \"cost\": 150,\n" +
                                "    \"productName\": \"laboris tempor\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        Product updatedProductFromPersistence = productRepository.getById(Long.valueOf(productId));
        assertEquals(10, updatedProductFromPersistence.getAmountAvailable());
        assertEquals(150, updatedProductFromPersistence.getCost());
        assertEquals("laboris tempor", updatedProductFromPersistence.getProductName());
    }

    @Test
    public void shouldDeleteProduct() throws Exception {
        MvcResult createdProduct = mockMvc.perform(MockMvcRequestBuilders.post("/product")
                        .with(httpBasic(this.seller.getUsername(), "seller"))
                        .content("{\n" +
                                "    \"amountAvailable\": 4,\n" +
                                "    \"cost\": 100,\n" +
                                "    \"productName\": \"laboris tempor\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String productId = createdProduct.getResponse().getContentAsString();
        mockMvc.perform(MockMvcRequestBuilders.delete("/product/" + productId)
                        .with(httpBasic(this.seller.getUsername(), "seller"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        Optional<Product> deletedProductFromPersistence = productRepository.findById(Long.valueOf(productId));
        assertFalse(deletedProductFromPersistence.isPresent());
    }

    @Test
    public void shouldGetProduct() throws Exception {
        MvcResult createdProduct = mockMvc.perform(MockMvcRequestBuilders.post("/product")
                        .with(httpBasic(this.seller.getUsername(), "seller"))
                        .content("{\n" +
                                "    \"amountAvailable\": 4,\n" +
                                "    \"cost\": 100,\n" +
                                "    \"productName\": \"laboris tempor\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String productId = createdProduct.getResponse().getContentAsString();
        mockMvc.perform(MockMvcRequestBuilders.get("/product/" + productId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.amountAvailable", Is.is(4)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cost", Is.is(100)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productName", Is.is("laboris tempor")));
    }
}