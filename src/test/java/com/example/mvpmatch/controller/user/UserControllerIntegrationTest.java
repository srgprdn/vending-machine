package com.example.mvpmatch.controller.user;

import com.example.mvpmatch.model.User;
import com.example.mvpmatch.model.UserRole;
import com.example.mvpmatch.repository.UserRepository;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static com.example.mvpmatch.validation.ValidationMessages.DUPLICATE_USERNAME_MESSAGE;
import static com.example.mvpmatch.validation.ValidationMessages.INVALID_USER_ROLE_MESSAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class UserControllerIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldCreateBuyerOnUserPOST() throws Exception {
        String user = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"buyer\"," +
                "    \"username\": \"buyer\"" +
                "}";
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(user)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String userId = mvcResult.getResponse().getContentAsString();
        User createdUser = userRepository.getById(Long.parseLong(userId));

        assertEquals("buyer", createdUser.getUsername());
        assertEquals(UserRole.BUYER, createdUser.getRole());
        assertEquals(0, createdUser.getDeposit());
    }

    @Test
    public void shouldCreateSellerOnUserPOST() throws Exception {
        String user = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"seller\"" +
                "}";
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(user)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String userId = mvcResult.getResponse().getContentAsString();
        User createdUser = userRepository.getById(Long.parseLong(userId));
        assertEquals("seller", createdUser.getUsername());
        assertEquals(UserRole.SELLER, createdUser.getRole());
        assertEquals(0, createdUser.getDeposit());
    }

    @Test
    public void shouldValidateRoleOnUserPOST() throws Exception {
        String user = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"not_a_valid_role\"," +
                "    \"username\": \"seller\"" +
                "}";
        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(user)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.reason", Is.is(INVALID_USER_ROLE_MESSAGE)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldRequireUniqueUsername() throws Exception {
        String user = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"sergiu\"" +
                "}";
        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(user)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(user)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.reason", Is.is(DUPLICATE_USERNAME_MESSAGE)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldRequireBasicAuthForDelete() throws Exception {
        String victim = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"victim2\"" +
                "}";
        MvcResult victimCreatedResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(victim)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String victimId = victimCreatedResult.getResponse().getContentAsString();

        mockMvc.perform(MockMvcRequestBuilders.delete("/user/" + victimId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void shouldAllowUserToDeleteItself() throws Exception {
        String user = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"sellerDelete\"" +
                "}";
        MvcResult userCreatedResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(user)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String userId = userCreatedResult.getResponse().getContentAsString();

        mockMvc.perform(MockMvcRequestBuilders.delete("/user/" + userId)
                        .with(httpBasic("sellerDelete", "pass"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void shouldNOTAllowUserToDeleteOtherUser() throws Exception {
        String victim = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"victim1\"" +
                "}";
        MvcResult victimCreatedResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(victim)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String attacker = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"attacker\"" +
                "}";
        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(attacker)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String victimId = victimCreatedResult.getResponse().getContentAsString();

        mockMvc.perform(MockMvcRequestBuilders.delete("/user/" + victimId)
                        .with(httpBasic("attacker", "pass"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void shouldRequireBasicAuthForGET() throws Exception {
        String victim = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"victim3\"" +
                "}";
        MvcResult victimCreatedResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(victim)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String victimId = victimCreatedResult.getResponse().getContentAsString();

        mockMvc.perform(MockMvcRequestBuilders.get("/user/" + victimId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void shouldAllowUserToGETItself() throws Exception {
        String user = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"sellerGET\"" +
                "}";
        MvcResult userCreatedResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(user)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String userId = userCreatedResult.getResponse().getContentAsString();

        mockMvc.perform(MockMvcRequestBuilders.get("/user/" + userId)
                        .with(httpBasic("sellerGET", "pass"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void shouldNOTAllowUserToGETOtherUser() throws Exception {
        String victim = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"victim4\"" +
                "}";
        MvcResult victimCreatedResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(victim)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String attacker = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"attacker2\"" +
                "}";
        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(attacker)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String victimId = victimCreatedResult.getResponse().getContentAsString();

        mockMvc.perform(MockMvcRequestBuilders.get("/user/" + victimId)
                        .with(httpBasic("attacker2", "pass"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void shouldRequireBasicAuthForPUT() throws Exception {
        String victim = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"victim5\"" +
                "}";
        MvcResult victimCreatedResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(victim)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String victimId = victimCreatedResult.getResponse().getContentAsString();

        mockMvc.perform(MockMvcRequestBuilders.put("/user/" + victimId)
                        .content(victim)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void shouldAllowUserToPUTItself() throws Exception {
        String user = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"sellerPUT\"" +
                "}";
        MvcResult userCreatedResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(user)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String userId = userCreatedResult.getResponse().getContentAsString();

        mockMvc.perform(MockMvcRequestBuilders.put("/user/" + userId)
                        .with(httpBasic("sellerPUT", "pass"))
                        .content("{" +
                                "    \"password\": \"pass\"," +
                                "    \"role\": \"seller\"," +
                                "    \"username\": \"sellerBLABLA\"" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void shouldNOTAllowUserToPUTOtherUser() throws Exception {
        String victim = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"victim6\"" +
                "}";
        MvcResult victimCreatedResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(victim)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String attacker = "{" +
                "    \"password\": \"pass\"," +
                "    \"role\": \"seller\"," +
                "    \"username\": \"attacker3\"" +
                "}";
        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .content(attacker)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String victimId = victimCreatedResult.getResponse().getContentAsString();

        mockMvc.perform(MockMvcRequestBuilders.put("/user/" + victimId)
                        .with(httpBasic("attacker3", "pass"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }
    //TODO: tests for PUT /user with various validation errors
}