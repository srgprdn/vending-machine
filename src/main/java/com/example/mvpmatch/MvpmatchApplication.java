package com.example.mvpmatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvpmatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvpmatchApplication.class, args);
	}

}
