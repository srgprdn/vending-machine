package com.example.mvpmatch.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DivisibleByValidator  implements ConstraintValidator<DivisibleBy, Long> {

    private long divisor;

    @Override
    public void initialize(DivisibleBy constraintAnnotation) {
        divisor = constraintAnnotation.divisor();
    }

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        return value % divisor == 0;
    }
}
