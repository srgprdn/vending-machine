package com.example.mvpmatch.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Constraint(validatedBy = DivisibleByValidator.class)
public @interface DivisibleBy {
    String NOT_DIVISIBLE_MESSAGE = "NOT_DIVISIBLE_MESSAGE";
    long divisor();
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String message() default NOT_DIVISIBLE_MESSAGE;
}