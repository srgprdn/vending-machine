package com.example.mvpmatch.controller.user;

class UserResponse {
    private long id;
    private String username;
    private long deposit;
    private String userRole;

    public UserResponse() {}

    public UserResponse(long id, String username, long deposit, String userRole) {
        this.id = id;
        this.username = username;
        this.deposit = deposit;
        this.userRole = userRole;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public long getDeposit() {
        return deposit;
    }

    public String getUserRole() {
        return userRole;
    }
}
