package com.example.mvpmatch.controller.user;

import com.example.mvpmatch.controller.common.ErrorResponse;
import com.example.mvpmatch.exception.DataNotFoundException;
import com.example.mvpmatch.exception.DataValidationException;
import com.example.mvpmatch.exception.DuplicateUsernameException;
import com.example.mvpmatch.service.user.UserDetails;
import com.example.mvpmatch.service.user.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

import static com.example.mvpmatch.validation.ValidationMessages.DUPLICATE_USERNAME_MESSAGE;
import static com.example.mvpmatch.validation.ValidationMessages.USER_NOT_FOUND_MESSAGE;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostMapping("/user")
    public long create(@Valid @RequestBody UserRequest userRequest) {
        UserDetails userDetails = toUserDetails(userRequest);
        return userService.save(userDetails);
    }

    @Operation(security = @SecurityRequirement(name = "basicAuth"))
    @PutMapping("/user/{userId}")
    public void update(@PathVariable("userId") long userId, @Valid @RequestBody UserRequest userUpdateRequest) {
        userService.update(userId, toUserDetails(userUpdateRequest));
    }

    @Operation(security = @SecurityRequirement(name = "basicAuth"))
    @GetMapping("/user/{userId}")
    public UserResponse retrieve(@PathVariable("userId") long userId) {
        return toUserResponse(userService.findById(userId));
    }

    @Operation(security = @SecurityRequirement(name = "basicAuth"))
    @DeleteMapping("/user/{userId}")
    public void delete(@PathVariable("userId") long userId) {
        userService.deleteById(userId);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DataNotFoundException.class)
    public ErrorResponse handleNotFound(DataNotFoundException ex) {
        return ErrorResponse.withReason(USER_NOT_FOUND_MESSAGE);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DuplicateUsernameException.class)
    public ErrorResponse handleNotFound(DuplicateUsernameException ex) {
        return ErrorResponse.withReason(DUPLICATE_USERNAME_MESSAGE);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DataValidationException.class)
    public ErrorResponse handleDataValidation(DataValidationException ex) {
        return ErrorResponse.withReason(ex.getReason());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ErrorResponse.withValidationErrors(errors);
    }

    private UserResponse toUserResponse(UserDetails userDetails) {
        return new UserResponse(userDetails.getId(), userDetails.getUsername(), userDetails.getDeposit(), userDetails.getRole());
    }

    private UserDetails toUserDetails(UserRequest userRequest) {
        UserDetails userDetails = new UserDetails();
        userDetails.setUsername(userRequest.getUsername());
        userDetails.setPassword(bCryptPasswordEncoder.encode(userRequest.getPassword()));
        userDetails.setRole(userRequest.getRole().toUpperCase());
        return userDetails;
    }
}
