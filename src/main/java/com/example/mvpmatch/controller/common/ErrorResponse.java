package com.example.mvpmatch.controller.common;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class ErrorResponse {
    private Map<String, String> errors;
    private final String reason;

    public ErrorResponse(Map<String, String> errors, String reason) {
        this.errors = errors;
        this.reason = reason;
    }

    public static ErrorResponse withValidationErrors(Map<String, String> errors) {
        return new ErrorResponse(errors, StringUtils.EMPTY);
    }

    public static ErrorResponse withReason(String reason) {
        return new ErrorResponse(Map.of(), reason);
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    public String getReason() {
        return reason;
    }
}
