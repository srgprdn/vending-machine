package com.example.mvpmatch.controller.vending;

import com.example.mvpmatch.service.vending.BuyStats;

class BuyResponse {
    private long amount;
    private String productName;
    private long remainingDeposit;
    private long transactionCost;

    public static BuyResponse from(BuyStats buyStats) {
        BuyResponse response = new BuyResponse();
        response.setAmount(buyStats.getAmount());
        response.setProductName(buyStats.getProductName());
        response.setRemainingDeposit(buyStats.getRemainingDeposit());
        response.setTransactionCost(buyStats.getTransactionCost());
        return response;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getRemainingDeposit() {
        return remainingDeposit;
    }

    public void setRemainingDeposit(long remainingDeposit) {
        this.remainingDeposit = remainingDeposit;
    }

    public long getTransactionCost() {
        return transactionCost;
    }

    public void setTransactionCost(long transactionCost) {
        this.transactionCost = transactionCost;
    }
}
