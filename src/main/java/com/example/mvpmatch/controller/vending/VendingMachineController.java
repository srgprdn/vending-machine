package com.example.mvpmatch.controller.vending;

import com.example.mvpmatch.controller.common.ErrorResponse;
import com.example.mvpmatch.exception.DataNotFoundException;
import com.example.mvpmatch.exception.DataValidationException;
import com.example.mvpmatch.security.UserDetailsImpl;
import com.example.mvpmatch.service.vending.BuyStats;
import com.example.mvpmatch.service.vending.VendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

import static com.example.mvpmatch.validation.ValidationMessages.USER_OR_PRODUCT_NOT_FOUND_MESSAGE;

@RestController
public class VendingMachineController {

    @Autowired
    private VendingService vendingService;

    @PostMapping("/deposit")
    public void deposit(@RequestBody @Valid DepositRequest depositRequest, Authentication authentication) {
        UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
        vendingService.deposit(authenticatedUser.getId(), depositRequest.getDepositAmount());
    }

    @PostMapping("/buy")
    public BuyResponse buy(@RequestBody @Valid BuyRequest buyRequest, Authentication authentication) {
        UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
        BuyStats stats = vendingService.buy(authenticatedUser.getId(), buyRequest.getProductId(), buyRequest.getAmount());
        return BuyResponse.from(stats);
    }

    @PostMapping("/reset")
    public Map<Long, Long> reset(Authentication authentication) {
        UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
        return vendingService.reset(authenticatedUser.getId());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DataNotFoundException.class)
    public ErrorResponse handleNotFound(DataNotFoundException ex) {
        return ErrorResponse.withReason(USER_OR_PRODUCT_NOT_FOUND_MESSAGE);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DataValidationException.class)
    public ErrorResponse handleDataValidation(DataValidationException ex) {
        return ErrorResponse.withReason(ex.getReason());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ErrorResponse.withValidationErrors(errors);
    }
}
