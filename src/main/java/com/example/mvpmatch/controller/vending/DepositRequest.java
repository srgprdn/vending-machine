package com.example.mvpmatch.controller.vending;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

class DepositRequest {

    @NotNull
    @Min(value = 5)
    private long depositAmount;

    public long getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(long depositAmount) {
        this.depositAmount = depositAmount;
    }
}
