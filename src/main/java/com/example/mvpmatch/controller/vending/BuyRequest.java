package com.example.mvpmatch.controller.vending;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

class BuyRequest {

    @NotNull
    private long productId;

    @Min(value = 1)
    private long amount;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
