package com.example.mvpmatch.controller.product;

import com.example.mvpmatch.service.product.ProductDetails;

class ProductResponse {
    private long id;
    private long amountAvailable;
    private long cost;
    private String productName;

    public ProductResponse() {}

    private ProductResponse(long id, long amountAvailable, long cost, String productName) {
        this.id = id;
        this.amountAvailable = amountAvailable;
        this.cost = cost;
        this.productName = productName;
    }

    public static ProductResponse from(ProductDetails productDetails) {
        ProductResponse response = new ProductResponse(
                productDetails.getId(),
                productDetails.getAmountAvailable(),
                productDetails.getCost(),
                productDetails.getProductName()
        );
        return response;
    }

    public long getAmountAvailable() {
        return amountAvailable;
    }

    public long getCost() {
        return cost;
    }

    public String getProductName() {
        return productName;
    }

    public long getId() {
        return id;
    }
}
