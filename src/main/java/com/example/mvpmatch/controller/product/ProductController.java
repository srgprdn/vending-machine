package com.example.mvpmatch.controller.product;

import com.example.mvpmatch.controller.common.ErrorResponse;
import com.example.mvpmatch.exception.DataNotFoundException;
import com.example.mvpmatch.security.UserDetailsImpl;
import com.example.mvpmatch.service.product.ProductDetails;
import com.example.mvpmatch.service.product.ProductService;
import com.example.mvpmatch.service.user.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

import static com.example.mvpmatch.validation.ValidationMessages.PRODUCT_NOT_FOUND_MESSAGE;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private UserService userService;

    @Operation(security = @SecurityRequirement(name = "basicAuth"))
    @PostMapping("/product")
    public long create(@Valid @RequestBody ProductRequest productRequest, Authentication authentication) {
        UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
        return productService.save(authenticatedUser.getId(), toProductDetails(productRequest));
    }

    @Operation(security = @SecurityRequirement(name = "basicAuth"))
    @PutMapping("/product/{productId}")
    public void update(@PathVariable("productId") long productId, @Valid @RequestBody ProductRequest productUpdateRequest) {
        productService.update(productId, toProductDetails(productUpdateRequest));
    }

    @GetMapping("/product/{productId}")
    public ProductResponse retrieve(@PathVariable("productId") long productId) {
        return ProductResponse.from(productService.findById(productId));
    }

    @Operation(security = @SecurityRequirement(name = "basicAuth"))
    @DeleteMapping("/product/{productId}")
    public void delete(@PathVariable("productId") long productId) {
        productService.deleteById(productId);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DataNotFoundException.class)
    public ErrorResponse handleNotFound(DataNotFoundException ex) {
        return ErrorResponse.withReason(PRODUCT_NOT_FOUND_MESSAGE);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ErrorResponse.withValidationErrors(errors);
    }

    private ProductDetails toProductDetails(ProductRequest productRequest) {
        ProductDetails productDetails = new ProductDetails();
        productDetails.setAmountAvailable(productRequest.getAmountAvailable());
        productDetails.setCost(productRequest.getCost());
        productDetails.setProductName(productRequest.getProductName());
        return productDetails;
    }
}
