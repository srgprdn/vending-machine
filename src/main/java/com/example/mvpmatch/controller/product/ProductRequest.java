package com.example.mvpmatch.controller.product;

import com.example.mvpmatch.validation.DivisibleBy;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

class ProductRequest {
    @NotNull
    private String productName;
    @NotNull
    @DivisibleBy(divisor = 5)
    private long cost;
    @NotNull
    @Min(value = 0)
    private long amountAvailable;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public long getAmountAvailable() {
        return amountAvailable;
    }

    public void setAmountAvailable(long amountAvailable) {
        this.amountAvailable = amountAvailable;
    }
}
