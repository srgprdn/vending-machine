package com.example.mvpmatch.repository;

import com.example.mvpmatch.model.Cash;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CashRepository extends JpaRepository<Cash, Long> {
}