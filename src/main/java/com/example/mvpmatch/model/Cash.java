package com.example.mvpmatch.model;

import javax.persistence.*;

@Entity
@Table
public class Cash {

    @Id
    @Column(updatable = false, nullable = false)
    private long denomination;

    @Column(nullable = false)
    private long quantity;

    @Version
    private long version;

    private Cash() {
    }

    public long getDenomination() {
        return denomination;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getVersion() {
        return version;
    }
}