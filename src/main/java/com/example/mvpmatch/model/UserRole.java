package com.example.mvpmatch.model;


public enum UserRole {
    BUYER,
    SELLER
}
