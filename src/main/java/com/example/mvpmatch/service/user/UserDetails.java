package com.example.mvpmatch.service.user;

import com.example.mvpmatch.model.User;

public class UserDetails {
    private String username;
    private String password;
    private String role;
    private long id;
    private long deposit;

    public static UserDetails from(User user) {
        UserDetails returned = new UserDetails();
        returned.setId(user.getId());
        returned.setPassword(user.getPassword());
        returned.setUsername(user.getUsername());
        returned.setRole(user.getRole().name());
        returned.setDeposit(user.getDeposit());
        return returned;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDeposit() {
        return deposit;
    }

    public void setDeposit(long deposit) {
        this.deposit = deposit;
    }
}
