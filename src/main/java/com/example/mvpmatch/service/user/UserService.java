package com.example.mvpmatch.service.user;

import com.example.mvpmatch.exception.DataNotFoundException;
import com.example.mvpmatch.exception.DataValidationException;
import com.example.mvpmatch.exception.DuplicateUsernameException;
import com.example.mvpmatch.model.User;
import com.example.mvpmatch.model.UserRole;
import com.example.mvpmatch.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.example.mvpmatch.validation.ValidationMessages.INVALID_USER_ROLE_MESSAGE;

@Component
public class UserService {

    @Autowired
    private UserRepository userRepository;

    private static UserRole parseUserRole(String role) {
        try {
            return UserRole.valueOf(role);
        } catch (IllegalArgumentException e) {
            throw new DataValidationException(INVALID_USER_ROLE_MESSAGE);
        }
    }

    private static User copyUpdatableDetails(User existingUser, UserDetails updatedUser) {
        existingUser.setRole(parseUserRole(updatedUser.getRole()));
        existingUser.setPassword(updatedUser.getPassword());
        existingUser.setUsername(updatedUser.getUsername());
        return existingUser;
    }

    public long save(UserDetails userDetails) {
        User user = new User();
        user.setUsername(userDetails.getUsername());
        user.setPassword(userDetails.getPassword());
        user.setRole(parseUserRole(userDetails.getRole()));
        try {
            User savedUser = userRepository.save(user);
            return savedUser.getId();
        } catch (Exception e) {
            throw new DuplicateUsernameException();
        }
    }

    public void update(long id, UserDetails toBeUpdated) {
        Optional<User> existingUser = userRepository.findById(id);
        User updatedUser = existingUser
                .map(u -> copyUpdatableDetails(u, toBeUpdated))
                .orElseThrow(DataNotFoundException::new);
        userRepository.save(updatedUser);
    }

    public void deleteById(long id) {
        try {
            userRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new DataNotFoundException();
        }
    }

    public UserDetails findById(long id) {
        User user = userRepository.findById(id).orElseThrow(DataNotFoundException::new);
        return UserDetails.from(user);
    }

    public UserDetails findByUsername(String username) {
        return UserDetails.from(userRepository.findByUsername(username));
    }
}
