package com.example.mvpmatch.service.vending;

import com.example.mvpmatch.exception.DataNotFoundException;
import com.example.mvpmatch.exception.DataValidationException;
import com.example.mvpmatch.exception.NotEnoughCashAvailableException;
import com.example.mvpmatch.model.Cash;
import com.example.mvpmatch.model.Product;
import com.example.mvpmatch.model.User;
import com.example.mvpmatch.repository.CashRepository;
import com.example.mvpmatch.repository.ProductRepository;
import com.example.mvpmatch.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.mvpmatch.validation.ValidationMessages.*;

@Component
public class VendingService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CashRepository cashRepository;
    @Autowired
    private CashDispenserCalculator greedyCashDispenserCalculator;

    @Transactional
    public void deposit(long userId, long depositAmount) {
        User userToDeposit = userRepository.findById(userId).orElseThrow(DataNotFoundException::new);
        userToDeposit.setDeposit(userToDeposit.getDeposit() + depositAmount);
        userRepository.save(userToDeposit);
        Cash cash = cashRepository
                .findById(depositAmount)
                .orElseThrow(() -> new DataValidationException(INVALID_DENOMINATION_MESSAGE));
        cash.setQuantity(cash.getQuantity() + 1);
        cashRepository.save(cash);
    }

    @Transactional
    public BuyStats buy(long userId, long productId, long amount) {
        User buyer = userRepository.findById(userId).orElseThrow(DataNotFoundException::new);
        Product product = productRepository.findById(productId).orElseThrow(DataNotFoundException::new);
        User seller = product.getUser();
        long initialProductAmount = product.getAmountAvailable();
        long initialBuyerDeposit = buyer.getDeposit();
        long transactionCost = product.getCost() * amount;
        if (initialProductAmount < amount) {
            throw new DataValidationException(INSUFFICIENT_PRODUCT_AMOUNT_MESSAGE);
        }
        if (initialBuyerDeposit < transactionCost) {
            throw new DataValidationException(INSUFFICIENT_BALANCE_MESSAGE);
        }
        buyer.setDeposit(initialBuyerDeposit - transactionCost);
        seller.setDeposit(seller.getDeposit() + transactionCost);
        product.setAmountAvailable(initialProductAmount - amount);
        userRepository.save(buyer);
        productRepository.save(product);
        return new BuyStats(product.getProductName(), amount, transactionCost, buyer.getDeposit());
    }

    @Transactional
    public Map<Long, Long> reset(long userId) {
        //todo show message informing the user of max dispensable amount and allow withdrawing a smaller amount
        long userBalance = resetUserBalance(userId);
        List<Cash> availableCash = cashRepository.findAll();
        Map<Long, Long> denominationToQuantity = null;
        try {
            denominationToQuantity = greedyCashDispenserCalculator.calculateCashToDispense(userBalance, toDenominationQuantityMap(availableCash));
        } catch (NotEnoughCashAvailableException e) {
            throw new DataValidationException(INSUFFICIENT_CASH_AVAILABLE_MESSAGE);
        }
        updateCashBalance(availableCash, denominationToQuantity);
        return denominationToQuantity;
    }

    private Map<Long, Long> toDenominationQuantityMap(List<Cash> availableCashes) {
        Map<Long, Long> returned = new HashMap<>();
        for (Cash availableCash : availableCashes) {
            returned.put(availableCash.getDenomination(), availableCash.getQuantity());
        }
        return returned;
    }

    private void updateCashBalance(List<Cash> availableCash, Map<Long, Long> denominationToAmountMap) {
        for (Cash cash : availableCash) {
            if (denominationToAmountMap.containsKey(cash.getDenomination())) {
                cash.setQuantity(cash.getQuantity() - denominationToAmountMap.get(cash.getDenomination()));
            }
            cashRepository.save(cash);
        }
    }

    private long resetUserBalance(long userId) {
        User userToReset = userRepository.findById(userId).orElseThrow(DataNotFoundException::new);
        long userDeposit = userToReset.getDeposit();
        userToReset.setDeposit(0);
        userRepository.save(userToReset);
        return userDeposit;
    }
}
