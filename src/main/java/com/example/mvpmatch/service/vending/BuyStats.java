package com.example.mvpmatch.service.vending;

public class BuyStats {
    private String productName;
    private long amount;
    private long transactionCost;
    private long remainingDeposit;

    public BuyStats(String productName, long amount, long transactionCost, long remainingDeposit) {
        this.productName = productName;
        this.amount = amount;
        this.transactionCost = transactionCost;
        this.remainingDeposit = remainingDeposit;
    }

    public String getProductName() {
        return productName;
    }

    public long getAmount() {
        return amount;
    }

    public long getTransactionCost() {
        return transactionCost;
    }

    public long getRemainingDeposit() {
        return remainingDeposit;
    }
}
