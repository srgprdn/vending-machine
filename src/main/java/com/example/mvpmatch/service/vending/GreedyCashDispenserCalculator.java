package com.example.mvpmatch.service.vending;

import com.example.mvpmatch.exception.NotEnoughCashAvailableException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Greedy approach at dispensing the least amount of cash items. Finds the optimal solution for the cash system composed
 * of: 5,10,20,50,100; however for other coinage systems it might work sub-optimally or not be able to find a
 * solution at all.
 *
 * @see <a href="Change-making problem">https://en.wikipedia.org/wiki/Change-making_problem</a>
 * @see <a href="COMBINATORICS OF THE CHANGE-MAKING PROBLEM">https://arxiv.org/pdf/0801.0120.pdf</a>
 */
@Component
class GreedyCashDispenserCalculator implements CashDispenserCalculator {

    @Override
    public Map<Long, Long> calculateCashToDispense(final long totalToDispense, Map<Long, Long> denominationToQuantityMap) throws NotEnoughCashAvailableException {
        if (totalToDispense < 0) {
            // todo use a different exception
            throw new NotEnoughCashAvailableException();
        }
        Map<Long, Long> dispensed = new HashMap<>();
        long remainingToDispense = totalToDispense;
        List<Map.Entry<Long, Long>> sortedAvailableCash = denominationToQuantityMap.entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 0L)
                .sorted(Map.Entry.<Long, Long>comparingByKey().reversed())
                .collect(Collectors.toList());
        for (int i = 0; i < sortedAvailableCash.size(); i++) {
            Map.Entry<Long, Long> currentCashItem = sortedAvailableCash.get(i);
            Long cashItemValue = currentCashItem.getKey();
            Long cashItemQty = currentCashItem.getValue();
            while (remainingToDispense >= cashItemValue && cashItemQty > 0) {
                Long dispensedQtyForValue = dispensed.getOrDefault(cashItemValue, 0L);
                dispensed.put(cashItemValue, ++dispensedQtyForValue);
                remainingToDispense-=cashItemValue;
                cashItemQty--;
            }
        }
        if (remainingToDispense != 0) {
            throw new NotEnoughCashAvailableException();
        }
        return dispensed;
    }
}