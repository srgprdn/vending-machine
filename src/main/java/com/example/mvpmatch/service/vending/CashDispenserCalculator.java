package com.example.mvpmatch.service.vending;

import com.example.mvpmatch.exception.NotEnoughCashAvailableException;

import java.util.Map;

public interface CashDispenserCalculator {
    /**
     * Given an amount to dispense and the available cash in the form of denomination to amount map it will compute the
     * minimum number of cash items(coins, banknotes etc.) to dispense.
     *
     * @param totalToDispense           sum of cash to be dispensed
     * @param denominationToQuantityMap the available physical cash items that can be dispensed
     * @return mapping of denomination to quantity that must be dispensed to satisfy the total
     * @throws NotEnoughCashAvailableException when the amount to dispense cannot be satisfied by available cash
     */
    Map<Long, Long> calculateCashToDispense(long totalToDispense, Map<Long, Long> denominationToQuantityMap) throws NotEnoughCashAvailableException;
}
