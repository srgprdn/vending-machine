package com.example.mvpmatch.service.product;

import com.example.mvpmatch.exception.DataNotFoundException;
import com.example.mvpmatch.model.Product;
import com.example.mvpmatch.model.User;
import com.example.mvpmatch.repository.ProductRepository;
import com.example.mvpmatch.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserRepository userRepository;

    private static Product copyUpdatableDetails(Product existingProduct, ProductDetails updatedProduct) {
        existingProduct.setAmountAvailable(updatedProduct.getAmountAvailable());
        existingProduct.setCost(updatedProduct.getCost());
        existingProduct.setProductName(updatedProduct.getProductName());
        return existingProduct;
    }

    public long save(long userId, ProductDetails productDetails) {
        User seller = userRepository.findById(userId).orElseThrow(DataNotFoundException::new);
        Product product = new Product();
        product.setProductName(productDetails.getProductName());
        product.setCost(productDetails.getCost());
        product.setAmountAvailable(productDetails.getAmountAvailable());
        product.setUser(seller);
        Product savedProduct = productRepository.save(product);
        return savedProduct.getId();
    }

    public void update(long id, ProductDetails toBeUpdated) {
        Optional<Product> existingProduct = productRepository.findById(id);
        Product updatedProduct = existingProduct
                .map(u -> copyUpdatableDetails(u, toBeUpdated))
                .orElseThrow(DataNotFoundException::new);
        productRepository.save(updatedProduct);
    }

    public void deleteById(long id) {
        try {
            productRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new DataNotFoundException();
        }
    }

    public ProductDetails findById(long id) {
        Product product = productRepository.findById(id).orElseThrow(DataNotFoundException::new);
        return ProductDetails.from(product);
    }
}
