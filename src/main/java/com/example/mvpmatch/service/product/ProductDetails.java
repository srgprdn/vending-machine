package com.example.mvpmatch.service.product;

import com.example.mvpmatch.model.Product;

public class ProductDetails {
    private long amountAvailable;
    private long cost;
    private String productName;
    private long id;
    private long userId;

    public static ProductDetails from(Product product) {
        ProductDetails productDetails = new ProductDetails();
        productDetails.setProductName(product.getProductName());
        productDetails.setCost(product.getCost());
        productDetails.setAmountAvailable(product.getAmountAvailable());
        productDetails.setId(product.getId());
        productDetails.setUserId(product.getUser().getId());
        return productDetails;
    }

    public long getAmountAvailable() {
        return amountAvailable;
    }

    public void setAmountAvailable(long amountAvailable) {
        this.amountAvailable = amountAvailable;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    private void setUserId(long userId) {
        this.userId = userId;
    }
}
