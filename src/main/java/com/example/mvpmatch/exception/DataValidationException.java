package com.example.mvpmatch.exception;

public class DataValidationException extends RuntimeException {
    private String reason;

    public DataValidationException(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
