package com.example.mvpmatch.security;

import com.example.mvpmatch.exception.DataNotFoundException;
import com.example.mvpmatch.service.product.ProductDetails;
import com.example.mvpmatch.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component("productSecurity")
public class ProductSecurity {

    @Autowired
    private ProductService productService;

    public boolean ownedByAuthenticated(Authentication authentication, Long productId) {
        if (authentication.getPrincipal() instanceof UserDetailsImpl) {
            UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
            ProductDetails productById;
            try {
                productById = productService.findById(productId);
            } catch (DataNotFoundException e) {
                return false;
            }
            return productById.getUserId() == authenticatedUser.getId();
        }
        return false;
    }
}
