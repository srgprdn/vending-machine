package com.example.mvpmatch.security;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component("userSecurity")
public class UserSecurity {
    public boolean sameIdAsAuthenticated(Authentication authentication, Long userId) {
        if (authentication.getPrincipal() instanceof UserDetailsImpl) {
            UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
            return authenticatedUser.getId() == userId;
        }
        return false;
    }
}