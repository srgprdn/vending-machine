package com.example.mvpmatch.security;

import com.example.mvpmatch.model.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationProvider customAuthenticationProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()

                    .mvcMatchers(HttpMethod.POST, "/user").permitAll()
                    .mvcMatchers(HttpMethod.PUT, "/user/{userId}").access("@userSecurity.sameIdAsAuthenticated(authentication, #userId)")
                    .mvcMatchers(HttpMethod.DELETE, "/user/{userId}").access("@userSecurity.sameIdAsAuthenticated(authentication, #userId)")
                    .mvcMatchers(HttpMethod.GET, "/user/{userId}").access("@userSecurity.sameIdAsAuthenticated(authentication, #userId)")

                    .mvcMatchers(HttpMethod.POST, "/product").hasAuthority("SELLER")
                    .mvcMatchers(HttpMethod.PUT, "/product/{productId}").access("@productSecurity.ownedByAuthenticated(authentication, #productId)")
                    .mvcMatchers(HttpMethod.DELETE, "/product/{productId}").access("@productSecurity.ownedByAuthenticated(authentication, #productId)")
                    .mvcMatchers(HttpMethod.GET, "/product/{productId}").permitAll()

                    .mvcMatchers(HttpMethod.POST, "/deposit").hasAuthority("BUYER")
                    .mvcMatchers(HttpMethod.POST, "/buy").hasAuthority("BUYER")
                    .mvcMatchers(HttpMethod.POST, "/reset").hasAuthority("BUYER")
                .anyRequest().permitAll()
                    .and()
                .formLogin()
                    .permitAll()
                    .and()
                .logout()
                    .permitAll()
                    .and()
                    .httpBasic()
                    .and()
                .csrf()
                    // easier to test using swagger-ui
                    .disable()
                .headers()
                    // h2-console is using iframes
                    .frameOptions()
                    .sameOrigin()
                    .and()
                .authenticationProvider(customAuthenticationProvider);
    }
}
